/*package com.example.recyclerviewexample.database

import android.content.ContentValues
import android.content.Context
import android.database.sqlite.SQLiteDatabase
import android.database.sqlite.SQLiteOpenHelper
import com.example.recyclerviewexample.Producto
import com.example.recyclerviewexample.database.TournamentDBConstants.Companion.ID
import com.example.recyclerviewexample.database.TournamentDBConstants.Companion.NOMBRE_PRODUCTO
import com.example.recyclerviewexample.database.TournamentDBConstants.Companion.PRECIO_PRODUCTO
import com.example.recyclerviewexample.database.TournamentDBConstants.Companion.TABLE_PRODUCTO

class DBHelper(context: Context) :
        SQLiteOpenHelper(context, TournamentDBConstants.DBName, null, TournamentDBConstants.DBVersion) {


    override fun onCreate(db: SQLiteDatabase) {
        db.execSQL("create table if not exists $TABLE_PRODUCTO ($ID integer primary key, $NOMBRE_PRODUCTO text, $PRECIO_PRODUCTO text)")
    }

    override fun onUpgrade(db: SQLiteDatabase, oldVersion: Int, newVersion: Int) {
        if (oldVersion < newVersion) {
            db.execSQL("drop table $TABLE_PRODUCTO")
        }
    }

    fun insertProducto(producto: Producto): Boolean {
        val db = this.writableDatabase
        val contentValues = ContentValues()
        contentValues.put(NOMBRE_PRODUCTO, producto.nombre)
        contentValues.put(PRECIO_PRODUCTO, producto.precio)

        val result = db.insert(TABLE_PRODUCTO, null, contentValues).toInt()
        db.close()

        return result != -1
    }

    fun getProductos(): List<Producto> {
        val productos: ArrayList<Producto> = ArrayList()
        val db = this.readableDatabase
        var cursor = db.rawQuery("select * FROM $TABLE_PRODUCTO", null)
        if (cursor.moveToFirst()) {
            while (!cursor.isAfterLast) {
                val id = cursor.getInt(cursor.getColumnIndex(ID))
                val nombre = cursor.getString(cursor.getColumnIndex(NOMBRE_PRODUCTO))
                val precio = cursor.getString(cursor.getColumnIndex(PRECIO_PRODUCTO))

                productos.add(Producto(id, nombre, precio))
                cursor.moveToNext()
            }
        }

        cursor.close()
        db.close()

        return productos
    }

    fun updateProducto(producto: Producto) {
        val db = this.writableDatabase
        val contentValues = ContentValues()
        contentValues.put(NOMBRE_PRODUCTO, producto.nombre)
        contentValues.put(PRECIO_PRODUCTO, producto.precio)

        db.update(TABLE_PRODUCTO, contentValues, "$ID = ?", arrayOf(producto.id.toString()))
        db.close()
    }

    fun deleteProducto(producto: Producto) {
        val db = this.writableDatabase

        db.delete(TABLE_PRODUCTO, "$ID = ?", arrayOf(producto.id.toString()))

        db.close()
    }

}*/
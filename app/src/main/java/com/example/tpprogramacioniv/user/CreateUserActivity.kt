package com.example.tpprogramacioniv.user

import android.app.NotificationChannel
import android.app.NotificationManager
import android.app.PendingIntent
import android.content.Context
import android.content.Intent
import android.os.Build
import android.support.v7.app.AppCompatActivity
import android.os.Bundle
import android.support.v4.app.NotificationCompat
import android.support.v7.widget.Toolbar
import android.widget.*
import com.example.tpprogramacioniv.MainActivity
import com.example.tpprogramacioniv.R
import com.example.tpprogramacioniv.database.DBHelper

class CreateUserActivity : AppCompatActivity() {


    private lateinit var etUsername: TextView
    private lateinit var etPassword: TextView
    private lateinit var btnCreate: Button

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_create_user)

        setupUI()
        completeInfo()
        initializeToolbar()
        initializeGenerateBtn()
    }

    private fun insert() {
        val db = DBHelper(this)
        val username = etUsername.text.toString()
        val password = etPassword.text.toString()

        db.insert(
            User(
                0,
                username,
                password
            )
        )
    }
    private val CHANNEL_ID: String = "Agregar usuario"
    private fun showNotification() {
        val builder = NotificationCompat.Builder(this, CHANNEL_ID)
            .setSmallIcon(R.mipmap.ic_launcher_round)
            .setContentTitle("Usuarios")
            .setContentText("Se agrego un usuario")
            .setAutoCancel(true)
            .setPriority(NotificationCompat.PRIORITY_DEFAULT)

        val intent = Intent(this, MainActivity::class.java)
        val pendingIntent = PendingIntent.getActivity(this@CreateUserActivity,
            0, intent, PendingIntent.FLAG_UPDATE_CURRENT)
        builder.setContentIntent(pendingIntent)

        val managerCompat = getSystemService(NOTIFICATION_SERVICE) as NotificationManager
        managerCompat.notify(1234, builder.build())
    }


    private fun createNotificationChannel() {
        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.O) {
            val name = "Agregar usuario"
            val descriptionText = "Se notificara en el momento de agregar un usuario"
            val importance = NotificationManager.IMPORTANCE_DEFAULT
            val channel = NotificationChannel(CHANNEL_ID, name, importance).apply {
                description = descriptionText
            }

            val notificationManager: NotificationManager =
                getSystemService(Context.NOTIFICATION_SERVICE) as NotificationManager
            notificationManager.createNotificationChannel(channel)
        }
    }

    private fun initializeGenerateBtn() {
        btnCreate.setOnClickListener {
            if (completeInfo()) {
                insert()
                createNotificationChannel()
                showNotification()
                finish()
            } else {
                Toast.makeText(this, "Completar datos", Toast.LENGTH_LONG).show()
            }

        }
    }

    private fun completeInfo(): Boolean {
        return !etUsername.text.toString().isEmpty() &&
                !etPassword.text.toString().isEmpty()
    }

    private fun setupUI() {
        etUsername = findViewById(R.id.etUsername)
        etPassword = findViewById(R.id.etPassword)
        btnCreate = findViewById(R.id.btnCreate)
    }

    private fun initializeToolbar() {
        supportActionBar?.setTitle("Crear usuario")
        supportActionBar?.setDisplayHomeAsUpEnabled(true)
    }
}

package com.example.tpprogramacioniv.user

import java.io.Serializable

class User
    (
    var id: Int,
    var usuario: String,
    var contraseña: String
    ) : Serializable
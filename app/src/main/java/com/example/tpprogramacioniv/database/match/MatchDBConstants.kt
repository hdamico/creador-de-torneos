package com.example.tpprogramacioniv.database.match



class MatchDBConstants {

    companion object {

        //Tabla match
        val TABLE_MATCH = "Match"
        val ID = "Id"
        val EQUIPO_LOCAL = "Equipo_Local"
        val EQUIPO_VISITANTE = "Equipo_Visitante"
        val FECHA_INICIO = "Fecha_Inicio"
        val TOURNAMENT_ID = "tournament_id"
    }


}

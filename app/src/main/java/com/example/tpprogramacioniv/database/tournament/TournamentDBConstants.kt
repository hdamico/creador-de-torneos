package com.example.tpprogramacioniv.database.tournament



class TournamentDBConstants {

    companion object {

        //Tabla productos
        val TABLE_TOURNAMENT = "Tournament"
        val ID = "Id"
        val NOMBRE = "Nombre"
        val SEDE = "Sede"
        val FECHA_INICIO    = "FechaInicio"
        val FECHA_FIN       = "FechaFin"
    }


}

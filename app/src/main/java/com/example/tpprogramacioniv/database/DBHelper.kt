package com.example.tpprogramacioniv.database

import android.content.ContentValues
import android.content.Context
import android.database.sqlite.SQLiteDatabase
import android.database.sqlite.SQLiteOpenHelper
import com.example.tpprogramacioniv.database.match.MatchDBConstants
import com.example.tpprogramacioniv.database.tournament.TournamentDBConstants
import com.example.tpprogramacioniv.database.user.UserDBConstants
import com.example.tpprogramacioniv.tournament.Tournament
import com.example.tpprogramacioniv.match.Match
import com.example.tpprogramacioniv.user.User
import com.example.tpprogramacioniv.database.tournament.TournamentDBConstants.Companion.NOMBRE
import com.example.tpprogramacioniv.database.tournament.TournamentDBConstants.Companion.SEDE
import com.example.tpprogramacioniv.database.tournament.TournamentDBConstants.Companion.FECHA_FIN
import com.example.tpprogramacioniv.database.tournament.TournamentDBConstants.Companion.TABLE_TOURNAMENT
import com.example.tpprogramacioniv.database.match.MatchDBConstants.Companion.EQUIPO_LOCAL
import com.example.tpprogramacioniv.database.match.MatchDBConstants.Companion.EQUIPO_VISITANTE
import com.example.tpprogramacioniv.database.match.MatchDBConstants.Companion.TOURNAMENT_ID
import com.example.tpprogramacioniv.database.match.MatchDBConstants.Companion.TABLE_MATCH
import com.example.tpprogramacioniv.database.user.UserDBConstants.Companion.TABLE_USER
import com.example.tpprogramacioniv.database.user.UserDBConstants.Companion.USUARIO
import com.example.tpprogramacioniv.database.user.UserDBConstants.Companion.CONTRASEÑA

class DBHelper(context: Context) :

        SQLiteOpenHelper(context, DBConstants.DBName, null, DBConstants.DBVersion) {

    override fun onCreate(db: SQLiteDatabase) {
        db.execSQL("create table if not exists $TABLE_TOURNAMENT (${TournamentDBConstants.ID} integer primary key , $NOMBRE text, $SEDE text, ${TournamentDBConstants.FECHA_INICIO} text, $FECHA_FIN text)")
        db.execSQL("create table if not exists $TABLE_MATCH (${MatchDBConstants.ID} integer primary key, ${MatchDBConstants.FECHA_INICIO} text, $EQUIPO_LOCAL text, $EQUIPO_VISITANTE text, $TOURNAMENT_ID integer, FOREIGN KEY(TOURNAMENT_ID) REFERENCES tournaments(id) )")
        db.execSQL("create table if not exists $TABLE_USER (${UserDBConstants.ID} integer primary key , $USUARIO text, $CONTRASEÑA text)")
    }

    override fun onUpgrade(db: SQLiteDatabase, oldVersion: Int, newVersion: Int) {
        if (oldVersion < newVersion) {
            db.execSQL("drop table $TABLE_TOURNAMENT")
            db.execSQL("drop table $TABLE_MATCH")
            db.execSQL("drop table $TABLE_USER")
        }
    }

    // Tournament CRUD

    fun getAll(): List<Tournament> {
        val tournaments: ArrayList<Tournament> = ArrayList()
        val db = this.readableDatabase
        var cursor = db.rawQuery("select * FROM $TABLE_TOURNAMENT", null)
        if (cursor.moveToFirst()) {
            while (!cursor.isAfterLast) {
                val id = cursor.getInt(cursor.getColumnIndex(TournamentDBConstants.ID))
                val nombreTournament = cursor.getString(cursor.getColumnIndex(NOMBRE))
                val sedeTournament = cursor.getString(cursor.getColumnIndex(SEDE))
                val fecha_inicio = cursor.getString(cursor.getColumnIndex(TournamentDBConstants.FECHA_INICIO))
                val fecha_fin = cursor.getString(cursor.getColumnIndex(FECHA_FIN))
                tournaments.add(
                    Tournament(
                        id,
                        nombreTournament,
                        sedeTournament,
                        fecha_inicio,
                        fecha_fin
                    )
                )
                cursor.moveToNext()
            }
        }
        cursor.close()
        db.close()

        return  tournaments
    }

    fun insert(tournament: Tournament): Boolean {
        val db = this.writableDatabase
        val contentValues = ContentValues()
        contentValues.put(NOMBRE, tournament.nombre)
        contentValues.put(SEDE,tournament.sede)
        contentValues.put(TournamentDBConstants.FECHA_INICIO,tournament.fechaInicio)
        contentValues.put(FECHA_FIN,tournament.fechaFin)



        val result = db.insert(TABLE_TOURNAMENT, null, contentValues).toInt()
        db.close()

        return result != -1
    }

    fun delete(tournament: Tournament) {
        val db = this.writableDatabase
        db.delete(TABLE_TOURNAMENT, "${TournamentDBConstants.ID} = ?", arrayOf(tournament.id.toString()))
        db.close()
    }

    fun update(tournament: Tournament) {
        val db = this.writableDatabase
        val contentValues = ContentValues()
        contentValues.put(NOMBRE, tournament.nombre)
        contentValues.put(SEDE, tournament.sede)
        contentValues.put(TournamentDBConstants.FECHA_INICIO,tournament.fechaInicio)
        contentValues.put(FECHA_FIN,tournament.fechaFin)
        db.update(TABLE_TOURNAMENT, contentValues, "${TournamentDBConstants.ID} = ?", arrayOf(tournament.id.toString()))
        db.close()
    }

    // Match CRUD

    fun getAll(tournament_id: Int): List<Match> {
        val match: ArrayList<Match> = ArrayList()
        val db = this.readableDatabase
        var cursor = db.rawQuery("select * FROM $TABLE_MATCH WHERE tournament_id = $tournament_id", null)
        if (cursor.moveToFirst()) {
            while (!cursor.isAfterLast) {
                val id = cursor.getInt(cursor.getColumnIndex(MatchDBConstants.ID))
                val equipo_visitante = cursor.getString(cursor.getColumnIndex(EQUIPO_VISITANTE))
                val equipo_local = cursor.getString(cursor.getColumnIndex(EQUIPO_LOCAL))
                val fecha_inicio = cursor.getString(cursor.getColumnIndex(MatchDBConstants.FECHA_INICIO))
                match.add(Match(id, equipo_visitante, equipo_local,fecha_inicio,tournament_id))
                cursor.moveToNext()
            }
        }
        cursor.close()
        db.close()

        return  match
    }

    fun insert(match: Match): Boolean {
        val db = this.writableDatabase
        val contentValues = ContentValues()
        contentValues.put(EQUIPO_LOCAL, match.equipo_local)
        contentValues.put(EQUIPO_VISITANTE, match.equipo_visitante)
        contentValues.put(MatchDBConstants.FECHA_INICIO, match.fecha_inicio)
        contentValues.put(TOURNAMENT_ID, match.tournament_id)
        val result = db.insert(TABLE_MATCH, null, contentValues).toInt()
        db.close()

        return result != -1
    }

    fun delete(match: Match) {
        val db = this.writableDatabase
        db.delete(TABLE_MATCH, "${MatchDBConstants.ID} = ?", arrayOf(match.id.toString()))
        db.close()
    }

    fun update(match: Match) {
        val db = this.writableDatabase
        val contentValues = ContentValues()
        contentValues.put(EQUIPO_VISITANTE, match.equipo_visitante)
        contentValues.put(EQUIPO_LOCAL, match.equipo_local)
        contentValues.put(MatchDBConstants.FECHA_INICIO,match.fecha_inicio)
        db.update(TABLE_MATCH, contentValues, "${MatchDBConstants.ID} = ?", arrayOf(match.id.toString()))
        db.close()
    }

    // User Insert and Search

    fun insert(user: User): Boolean {
        val db = this.writableDatabase
        val contentValues = ContentValues()
        contentValues.put(USUARIO, user.usuario)
        contentValues.put(CONTRASEÑA, user.contraseña)
        val result = db.insert(TABLE_USER, null, contentValues).toInt()
        db.close()

        return result != -1
    }

    fun search(username: String, password: String): Boolean {
        val db = this.writableDatabase
        var cursor = db.rawQuery("SELECT * FROM $TABLE_USER WHERE usuario = '$username' AND contraseña = '$password'", null)
        return cursor.moveToFirst()
    }
}
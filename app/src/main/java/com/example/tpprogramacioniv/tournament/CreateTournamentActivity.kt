package com.example.tpprogramacioniv.tournament

import android.app.DatePickerDialog
import android.app.NotificationChannel
import android.app.NotificationManager
import android.app.PendingIntent
import android.content.Context
import android.content.Intent
import android.os.Build
import android.support.v7.app.AppCompatActivity
import android.os.Bundle
import android.support.v4.app.NotificationCompat
import android.support.v7.widget.Toolbar
import android.view.MenuItem
import android.widget.*
import com.example.tpprogramacioniv.MainActivity
import com.example.tpprogramacioniv.R
import com.example.tpprogramacioniv.database.DBHelper
import kotlinx.android.synthetic.main.insert_tournament.*
import java.util.*

class CreateTournamentActivity : AppCompatActivity() {


    private lateinit var spSedes: Spinner
    private val arrSedes   = arrayOf("Villa Urquiza", "Palermo", "Puerto Madero", "Belgrano")
    private lateinit var btnDate1: Button
    private lateinit var btnDate2: Button
    private lateinit var edtNombre: EditText
    private lateinit var edtFechaFin: TextView
    private lateinit var edtFechaInicio: TextView
    var modalidad: Int = MainActivity.AGREGAR
    var tournament: Tournament? = null

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.insert_tournament)
        modalidad = intent.extras.getInt(ListTournamentsActivity.MODALIDAD)
        tournament = intent.extras.getSerializable(ListTournamentsActivity.TOURNAMENT) as Tournament?

        setupUI()
        initializeToolbar()
        completeInfo()
        initializeGenerateBtn()
        initializeSpinner()
        DatePick(1)
        DatePick(2)
    }

    private fun insert() {
        val db = DBHelper(this)
        val nombreTournament = edtNombre.text.toString()
        val sedeTournament = spSedes.selectedItem.toString()
        val fechafinTournament    = edtFechaFin.text.toString()
        val fechainicioTournament = edtFechaInicio.text.toString()

        db.insert(
            Tournament(
                0,
                nombreTournament,
                sedeTournament,
                fechainicioTournament,
                fechafinTournament
            )
        )
    }
    private val CHANNEL_ID: String = "Agregar torneo"
    private fun showNotification() {
        val builder = NotificationCompat.Builder(this, CHANNEL_ID)
            .setSmallIcon(R.mipmap.ic_launcher_round)
            .setContentTitle("Torneos")
            .setContentText("Se agrego un torneo")
            .setAutoCancel(true)
            .setPriority(NotificationCompat.PRIORITY_DEFAULT)

        val intent = Intent(this, MainActivity::class.java)
        val pendingIntent = PendingIntent.getActivity(this@CreateTournamentActivity,
            0, intent, PendingIntent.FLAG_UPDATE_CURRENT)
        builder.setContentIntent(pendingIntent)

        val managerCompat = getSystemService(NOTIFICATION_SERVICE) as NotificationManager
        managerCompat.notify(1234, builder.build())
    }


    private fun createNotificationChannel() {
        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.O) {
            val name = "Agregar torneo"
            val descriptionText = "Se notificara en el momento de agregar un torneo"
            val importance = NotificationManager.IMPORTANCE_DEFAULT
            val channel = NotificationChannel(CHANNEL_ID, name, importance).apply {
                description = descriptionText
            }

            val notificationManager: NotificationManager =
                getSystemService(Context.NOTIFICATION_SERVICE) as NotificationManager
            notificationManager.createNotificationChannel(channel)
        }
    }



    private fun initializeGenerateBtn() {
        btnGenerateTournament.setOnClickListener {
            if (completeInfo()) {
                if (modalidad == ListTournamentsActivity.AGREGAR) {
                    insert()
                    createNotificationChannel()
                    showNotification()
                } else {
                    update()
                }

                finish()
            } else {
                Toast.makeText(this, "Completar datos", Toast.LENGTH_LONG).show()
            }

        }
    }

    private fun update() {
        val db = DBHelper(this)
        tournament?.nombre = edtNombre.text.toString()
        tournament?.sede = spSedes.selectedItem.toString()
        tournament?.fechaFin = edtFechaFin.text.toString()
        tournament?.fechaInicio = edtFechaInicio.text.toString()
        db.update(tournament!!)
    }

    private fun completeInfo(): Boolean {
        return !edtFechaFin.text.toString().isEmpty() &&
        !edtFechaFin.text.toString().isEmpty() &&
        !edtNombre.text.toString().isEmpty() &&
        !edtFechaFin.text.toString().isEmpty()
    }

    private fun setupUI() {
        edtNombre = findViewById(R.id.edtNombre)
        edtFechaFin = findViewById(R.id.edtFechaFin)
        edtFechaInicio = findViewById(R.id.edtFechaInicio)
        spSedes = findViewById(R.id.spSedes)
        btnDate1 = findViewById(R.id.btnDate1)
        btnDate2 = findViewById(R.id.btnDate2)

        if (modalidad == MainActivity.AGREGAR) {
            btnGenerateTournament.text = getString(R.string.agregar)
        } else {
            btnGenerateTournament.text = getString(R.string.update)

            edtNombre.setText(tournament?.nombre)
            edtFechaInicio.setText(tournament?.fechaInicio)
            edtFechaFin.setText(tournament?.fechaFin)
        }
    }

    private fun initializeToolbar() {
        supportActionBar?.setTitle("Crear torneo")
        supportActionBar?.setDisplayHomeAsUpEnabled(true)

    }

    private fun initializeSpinner() {

        val adapter: ArrayAdapter<String> = ArrayAdapter(
            this, android.R.layout.simple_spinner_dropdown_item, arrSedes)
        spSedes.adapter = adapter

    }

    override fun onOptionsItemSelected(item: MenuItem): Boolean {
        when (item.itemId) {
            // Respond to the action bar's Up/Home button
            android.R.id.home -> {
                onBackPressed()
                return true
            }
        }

        return super.onOptionsItemSelected(item)
    }

    private fun DatePick(button: Int) {
        val c = Calendar.getInstance();
        val año = c.get(Calendar.YEAR)
        val mes = c.get(Calendar.MONTH)
        val dia = c.get(Calendar.DAY_OF_MONTH)
        if(button == 1) {
            btnDate1.setOnClickListener {
                DatePickEvent("Inicio", año, mes, dia)
            }
        }else {
            btnDate2.setOnClickListener {
                DatePickEvent("Fin", año, mes, dia)
            }
        }

    }

    private fun DatePickEvent (fecha: String, año: Int, mes: Int, dia: Int) {
        val dpd = DatePickerDialog(this,DatePickerDialog.OnDateSetListener { _view, año, mes, dia ->
            if (fecha == "Inicio") {
                edtFechaInicio.setText("" + dia + "/" + mes + "/" + año)
            } else {
                edtFechaFin.setText("" + dia + "/" + mes + "/" + año)
            }
        },año,mes,dia)

        dpd.show()
    }
}

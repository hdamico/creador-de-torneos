package com.example.tpprogramacioniv.tournament

interface OnTournamentClickListener {
    fun onItemClick(tournament: Tournament)
}
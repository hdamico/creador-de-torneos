package com.example.tpprogramacioniv.tournament

import android.content.Intent
import android.os.AsyncTask
import android.support.v7.app.AppCompatActivity
import android.os.Bundle
import android.support.v7.app.AlertDialog
import android.support.v7.widget.RecyclerView
import android.support.v7.widget.Toolbar
import android.view.Menu
import android.view.MenuItem
import com.example.tpprogramacioniv.*
import com.example.tpprogramacioniv.database.DBHelper
import com.example.tpprogramacioniv.match.MatchActivity
import android.widget.Toast
import android.content.DialogInterface



class ListTournamentsActivity : AppCompatActivity() {
    companion object {
        val TOURNAMENT = "TOURNAMENT"
        val MODALIDAD = "MODALIDAD"
        val AGREGAR = 1
        val UPDATE = 2
    }

    lateinit var rvTournaments: RecyclerView
    lateinit var tournamentsAdapter: TournamentAdapter

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_list_tournaments)


        setupUI()
        initializeToolbar()
    }

    override fun onResume() {
        super.onResume()
        update()
    }

    private fun initializeToolbar() {
        supportActionBar?.setTitle("Torneos")

    }

    private fun setupUI() {
        rvTournaments = findViewById(R.id.rvTournaments)
        tournamentsAdapter = TournamentAdapter(ArrayList(), object : OnTournamentClickListener {
            override fun onItemClick(tournament: Tournament) {
                val builder = AlertDialog.Builder(this@ListTournamentsActivity)
                builder.setTitle(tournament.nombre)
                builder.setItems(arrayOf<CharSequence>("Ok", "Cambiar", "Fixture", "Eliminar")
                ) { dialog, which ->
                    // The 'which' argument contains the index position
                    // of the selected item
                    when (which) {
                        0 -> dialog.dismiss()
                        1 -> goToUpdate(tournament)
                        2 -> getTournamentMatches(tournament)
                        3 -> delete(tournament)
                    }
                }
                builder.create().show()
            }
        })
        rvTournaments.adapter = tournamentsAdapter
    }

    private fun getTournamentMatches(tournament:Tournament) {
        val intent = Intent(this, MatchActivity::class.java)
        intent.putExtra(TOURNAMENT, tournament.id)
        startActivity(intent)
    }

    private fun goToUpdate(tournament: Tournament) {
        val intent = Intent(this, CreateTournamentActivity::class.java)
        intent.putExtra(
            MODALIDAD,
            UPDATE
        )

        intent.putExtra(TOURNAMENT, tournament)
        startActivity(intent)
    }

    private fun delete(tournament: Tournament) {
        val db = DBHelper(this)
        db.delete(tournament)
        update()
    }

    private fun update() {
        GetTournament().execute()
    }

    private fun refresh(tournaments: List<Tournament>) {
        tournamentsAdapter.tournament = tournaments
        tournamentsAdapter.notifyDataSetChanged()
    }

    override fun onCreateOptionsMenu(menu: Menu?): Boolean {
        menuInflater.inflate(R.menu.main_menu, menu)
        return super.onCreateOptionsMenu(menu)
    }


    override fun onOptionsItemSelected(item: MenuItem): Boolean {
        if (item.itemId == R.id.addTournament) {
            val intent = Intent(this, CreateTournamentActivity::class.java)
            intent.putExtra(
                MainActivity.MODALIDAD,
                MainActivity.AGREGAR
            )
            startActivity(intent)
        }else if (item.itemId == R.id.logOut) {
            val intent = Intent(this, MainActivity::class.java)
            startActivity(intent)
            finish()
        }
        return super.onOptionsItemSelected(item)
    }

    inner class GetTournament : AsyncTask<Unit, Unit, List<Tournament>>() {

        override fun doInBackground(vararg params: Unit?): List<Tournament> {
            val db = DBHelper(this@ListTournamentsActivity)
            return db.getAll()
        }

        override fun onPostExecute(result: List<Tournament>) {
            super.onPostExecute(result)
            refresh(result)
        }

    }
}

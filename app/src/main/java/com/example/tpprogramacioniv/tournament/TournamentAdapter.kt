package com.example.tpprogramacioniv.tournament

import android.support.v7.widget.RecyclerView
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.TextView
import com.example.tpprogramacioniv.R

class TournamentAdapter(var tournament: List<Tournament>, var listener: OnTournamentClickListener)
    : RecyclerView.Adapter<TournamentAdapter.TournamentsViewHolder>() {

    override fun onCreateViewHolder(viewGroup: ViewGroup, viewType: Int): TournamentsViewHolder {
        val view = LayoutInflater.from(viewGroup.context)
            .inflate(R.layout.item_tournament, viewGroup, false)
        return TournamentsViewHolder(view)
    }

    override fun getItemCount(): Int {
        return tournament.size
    }

    override fun onBindViewHolder(holder: TournamentsViewHolder, position: Int) {
        holder.txtNombre.text = tournament[position].nombre
        holder.txtFechaInicio.text = tournament[position].fechaInicio
        holder.itemView.setOnClickListener {
            listener.onItemClick(tournament[position])
        }

    }

    class TournamentsViewHolder(view: View) : RecyclerView.ViewHolder(view) {
        var txtNombre: TextView = view.findViewById(R.id.txtNombre)
        var txtFechaInicio: TextView = view.findViewById(R.id.txtFechainicio)

    }
}
package com.example.tpprogramacioniv.tournament

import java.io.Serializable

class Tournament
    (
    var id: Int,
    var nombre: String,
    var sede: String,
    var fechaInicio: String  ,
    var fechaFin: String
    ) : Serializable
package com.example.tpprogramacioniv

import android.os.Bundle
import android.support.v7.app.AppCompatActivity

class SettingsActivity : AppCompatActivity() {

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_settings)

        showSettingsFragment()
    }

    private fun showSettingsFragment() {
        supportFragmentManager
            .beginTransaction()
            .add(R.id.container, SettingsFragment(), "SettingsFragment")
            .commit()
    }
}

package com.example.tpprogramacioniv.match

import java.io.Serializable

class Match(var id: Int,
            var equipo_visitante: String,
            var equipo_local: String,
            var fecha_inicio: String,
            var tournament_id: Int?
) : Serializable
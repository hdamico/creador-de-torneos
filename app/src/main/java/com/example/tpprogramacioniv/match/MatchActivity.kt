package com.example.tpprogramacioniv.match

import android.content.Intent
import android.os.AsyncTask
import android.os.Bundle
import android.support.v7.app.AlertDialog
import android.support.v7.app.AppCompatActivity
import android.support.v7.widget.RecyclerView
import android.support.v7.widget.Toolbar
import android.view.Menu
import android.view.MenuItem
import com.example.tpprogramacioniv.MainActivity
import com.example.tpprogramacioniv.R
import com.example.tpprogramacioniv.database.DBHelper
import com.example.tpprogramacioniv.tournament.ListTournamentsActivity





class MatchActivity : AppCompatActivity() {

    lateinit var rvMatch: RecyclerView
    var tournament_id: Int = 0


    companion object {
        var MODALIDAD = "MODALIDAD"
        var TOURNAMENT_ID = "TOURNAMENT"
        val MATCH = "MATCH"
        val AGREGAR = 1
        val UPDATE = 2
    }

    lateinit var matchAdapter: MatchAdapter
    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_match)

        setupUI()
        initializeToolbar()
    }

    override fun onResume() {
        super.onResume()

        matchAdapter.match = getAll(tournament_id)
        refresh()
        matchAdapter.notifyDataSetChanged()

    }

    private fun setupUI() {
        tournament_id = intent.extras.getSerializable(ListTournamentsActivity.TOURNAMENT) as Int
        rvMatch = findViewById(R.id.rvMatch)
        matchAdapter = MatchAdapter(ArrayList(), object : OnMatchClickListener {
            override fun onItemClick(match: Match) {
                val alertDialog = AlertDialog.Builder(this@MatchActivity)
                alertDialog.setTitle(match.equipo_local)
                    .setMessage("El equipo visitante es: ${match.equipo_visitante} El equipo local es: ${match.equipo_local}")
                    .setPositiveButton("OK", { dialog, _ -> dialog.dismiss() })
                    .setNeutralButton("UPDATE", { dialog, _ -> goToUpdate(match) })
                    .setNegativeButton("DELETE", { dialog, _ -> delete(match, tournament_id) })
                    .show()
            }
        })
        rvMatch.adapter = matchAdapter
    }




    private fun initializeToolbar() {
        supportActionBar?.setTitle("Partidos")
    }


    override fun onCreateOptionsMenu(menu: Menu?): Boolean {
        menuInflater.inflate(R.menu.main_menu, menu)
        return super.onCreateOptionsMenu(menu)
    }

    override fun onOptionsItemSelected(item: MenuItem): Boolean {
        if (item.itemId == R.id.addTournament) {
            val intent = Intent(this, CreateMatchActivity::class.java)
            intent.putExtra(
                MainActivity.MODALIDAD,
                MainActivity.AGREGAR
            )
            intent.putExtra(TOURNAMENT_ID,tournament_id)
            startActivity(intent)
        } else if (item.itemId == R.id.logOut) {
            startActivity(
                Intent(baseContext, MainActivity::class.java)
                    .addFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP or Intent.FLAG_ACTIVITY_SINGLE_TOP)
            )
            finishAffinity()
        }
        return super.onOptionsItemSelected(item)

    }

    private fun goToUpdate(match: Match) {
        val intent = Intent(this, CreateMatchActivity::class.java)
        intent.putExtra(MODALIDAD, UPDATE)

        intent.putExtra(MATCH, match)
        startActivity(intent)
    }

    private fun delete(match: Match, tournament_id: Int) {
        val db = DBHelper(this)
        db.delete(match)
        refresh()
    }

    private fun getAll(tournament_id:Int): List<Match> {
        val db = DBHelper(this)
        return db.getAll(tournament_id)
    }

    private fun refresh() {
        GetMatch().execute()
    }

    private fun refreshList(matches: List<Match>) {
        matchAdapter.match = matches
        matchAdapter.notifyDataSetChanged()
    }

    inner class GetMatch : AsyncTask<Unit, Unit, List<Match>>() {

        override fun doInBackground(vararg params: Unit?): List<Match> {
            val db = DBHelper(this@MatchActivity)
            return db.getAll(tournament_id)
        }

        override fun onPostExecute(result: List<Match>) {
            super.onPostExecute(result)
            refreshList(result)
        }

    }

}

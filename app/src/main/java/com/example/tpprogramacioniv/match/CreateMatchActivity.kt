package com.example.tpprogramacioniv.match

import android.app.DatePickerDialog
import android.app.NotificationChannel
import android.app.NotificationManager
import android.app.PendingIntent
import android.content.Context
import android.content.Intent
import android.os.Build
import android.support.v7.app.AppCompatActivity
import android.os.Bundle
import android.support.v4.app.NotificationCompat
import android.support.v7.widget.Toolbar
import android.view.MenuItem
import android.widget.*
import com.example.tpprogramacioniv.MainActivity
import com.example.tpprogramacioniv.R
import com.example.tpprogramacioniv.database.DBHelper
import com.example.tpprogramacioniv.tournament.Tournament
import kotlinx.android.synthetic.main.insert_match.*
import java.util.*

class CreateMatchActivity : AppCompatActivity() {


    private lateinit var btnDate1: Button
    private lateinit var edtEquipovisit: EditText
    private lateinit var edtEquipolocal: EditText
    private lateinit var edtFechaInicio: TextView
    var modalidad: Int = MainActivity.AGREGAR
    var tournament_id: Int? = 0
    var match: Match? = null

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.insert_match)
        modalidad = intent.extras.getInt(MatchActivity.MODALIDAD)
        tournament_id = intent.extras.getSerializable(MatchActivity.TOURNAMENT_ID) as Int?
        match = intent.extras.getSerializable(MatchActivity.MATCH) as Match?
        setupUI()
        initializeToolbar()
        completeInfo()
        initializeBtnGenerateMatch()
        DatePick()
    }

    private fun insert() {
        val db = DBHelper(this)
        val equipolocal = edtEquipolocal.text.toString()
        val equipovisit = edtEquipovisit.text.toString()
        val fechainicioMatch = edtFechaInicio.text.toString()
        val tournament_id = tournament_id

        db.insert(
             Match(
                0,
                 equipolocal,
                equipovisit,
                fechainicioMatch,
                 tournament_id

         )
        )
    }
    private val CHANNEL_ID: String = "Agregar partido"
    private fun showNotification() {
        val builder = NotificationCompat.Builder(this, CHANNEL_ID)
            .setSmallIcon(R.mipmap.ic_launcher_round)
            .setContentTitle("Partidos")
            .setContentText("Se agrego un partido")
            .setAutoCancel(true)
            .setPriority(NotificationCompat.PRIORITY_DEFAULT)

        val intent = Intent(this, MainActivity::class.java)
        val pendingIntent = PendingIntent.getActivity(this@CreateMatchActivity,
            0, intent, PendingIntent.FLAG_UPDATE_CURRENT)
        builder.setContentIntent(pendingIntent)

        val managerCompat = getSystemService(NOTIFICATION_SERVICE) as NotificationManager
        managerCompat.notify(1234, builder.build())
    }


    private fun createNotificationChannel() {
        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.O) {
            val name = "Agregar partido"
            val descriptionText = "Se notificara en el momento de agregar un partido"
            val importance = NotificationManager.IMPORTANCE_DEFAULT
            val channel = NotificationChannel(CHANNEL_ID, name, importance).apply {
                description = descriptionText
            }

            val notificationManager: NotificationManager =
                getSystemService(Context.NOTIFICATION_SERVICE) as NotificationManager
            notificationManager.createNotificationChannel(channel)
        }
    }

    private fun initializeBtnGenerateMatch() {
        btnGenerateMatch.setOnClickListener {
            if (completeInfo()) {
                if (modalidad == MatchActivity.AGREGAR) {
                    insert()
                    createNotificationChannel()
                    showNotification()
                } else {
                    update()
                }

                finish()
            } else {
                Toast.makeText(this, "Completar datos", Toast.LENGTH_LONG).show()
            }

        }
    }

    private fun update() {
        val db = DBHelper(this)
        match?.equipo_local = edtEquipolocal.text.toString()
        match?.equipo_visitante = edtEquipovisit.text.toString()
        match?.fecha_inicio = edtFechaInicio.text.toString()
        db.update(match!!)
    }

    private fun completeInfo(): Boolean {
        return !edtFechaInicio.text.toString().isEmpty() &&
        !edtEquipolocal.text.toString().isEmpty() &&
        !edtEquipovisit.text.toString().isEmpty() &&
        !edtFechaInicio.text.toString().isEmpty()
    }

    private fun setupUI() {
        edtEquipovisit = findViewById(R.id.edtEquipovisit)
        edtEquipolocal = findViewById(R.id.edtEquipolocal)
        edtFechaInicio = findViewById(R.id.edtFechaInicio)
        btnDate1 = findViewById(R.id.btnDate1)

        if (modalidad == MainActivity.AGREGAR) {
            btnGenerateMatch.text = getString(R.string.agregar)
        } else {
            btnGenerateMatch.text = getString(R.string.update)

            edtEquipolocal.setText(match?.equipo_local)
            edtEquipovisit.setText(match?.equipo_visitante)
            edtFechaInicio.setText(match?.fecha_inicio)
        }
    }

    private fun initializeToolbar() {
        supportActionBar?.setTitle("Crear partido")
        supportActionBar?.setDisplayHomeAsUpEnabled(true)

    }

    override fun onOptionsItemSelected(item: MenuItem): Boolean {
        when (item.itemId) {
            // Respond to the action bar's Up/Home button
            android.R.id.home -> {
                onBackPressed()
                return true
            }
        }

        return super.onOptionsItemSelected(item)
    }

    private fun DatePick() {
        val c = Calendar.getInstance();
        val año = c.get(Calendar.YEAR)
        val mes = c.get(Calendar.MONTH)
        val dia = c.get(Calendar.DAY_OF_MONTH)
        btnDate1.setOnClickListener {
            val dpd = DatePickerDialog(this,DatePickerDialog.OnDateSetListener { _view, año, mes, dia ->
                edtFechaInicio.setText("" + dia + "/" + mes + "/" + año)
            },año,mes,dia)

            dpd.show()
        }

    }

}

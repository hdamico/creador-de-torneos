package com.example.tpprogramacioniv.match

import android.support.v7.widget.RecyclerView
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.TextView
import com.example.tpprogramacioniv.R

class MatchAdapter(var match: List<Match>, var listener: OnMatchClickListener)
    : RecyclerView.Adapter<MatchAdapter.MatchViewHolder>() {

    override fun onCreateViewHolder(viewGroup: ViewGroup, viewType: Int): MatchViewHolder {
        val view = LayoutInflater.from(viewGroup.context)
            .inflate(R.layout.item_match, viewGroup, false)
        return MatchViewHolder(view)
    }

    override fun getItemCount(): Int {
        return match.size
    }

    override fun onBindViewHolder(holder: MatchViewHolder, position: Int) {
        holder.txtLocal.text = match[position].equipo_local
        holder.txtVisitante.text = match[position].equipo_visitante
        holder.txtFecha.text = match[position].fecha_inicio
        holder.itemView.setOnClickListener {
            listener.onItemClick(match[position])
        }
    }

    class MatchViewHolder(view: View) : RecyclerView.ViewHolder(view) {
        var txtLocal: TextView = view.findViewById(R.id.txtLocal)
        var txtVisitante: TextView = view.findViewById(R.id.txtVisitante)
        var txtFecha: TextView = view.findViewById(R.id.txtFecha)
    }

}
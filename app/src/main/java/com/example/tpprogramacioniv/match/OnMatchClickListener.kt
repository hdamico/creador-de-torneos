package com.example.tpprogramacioniv.match

interface OnMatchClickListener {
    fun onItemClick(match: Match)
}
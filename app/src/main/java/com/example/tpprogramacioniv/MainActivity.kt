package com.example.tpprogramacioniv

import android.content.Intent
import android.content.res.Configuration
import android.support.v7.app.AppCompatActivity
import android.os.Bundle
import android.preference.PreferenceManager
import android.support.design.widget.NavigationView
import android.support.v4.widget.DrawerLayout
import android.support.v7.app.ActionBarDrawerToggle
import android.support.v7.app.AlertDialog
import android.support.v7.widget.Toolbar
import android.view.Menu
import android.view.MenuItem
import android.widget.Button
import android.widget.EditText
import android.widget.Toast
import com.example.tpprogramacioniv.database.DBHelper
import com.example.tpprogramacioniv.tournament.ListTournamentsActivity
import com.example.tpprogramacioniv.user.CreateUserActivity

class MainActivity : AppCompatActivity(), NavigationView.OnNavigationItemSelectedListener {

    companion object {
        val MODALIDAD = "MODALIDAD"
        val AGREGAR = 1
    }
    private lateinit var username: EditText
    private lateinit var password: EditText
    private lateinit var logIn: Button
    private lateinit var createUser: Button
    private lateinit var drawer: DrawerLayout
    private lateinit var toggle: ActionBarDrawerToggle
    private lateinit var nombre: String
    private var saludar: Boolean = false

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        PreferenceManager.setDefaultValues(this, R.xml.settings, false)

        setContentView(R.layout.activity_main)

        setupUI()
        initilizeLogIn()
        initilizeCreateUser()
        initializeToolbar()
        asignarValoresSettings()
        getFirebaseNotifications()

        if (saludar) {
            Toast.makeText(this, "Hola $nombre", Toast.LENGTH_LONG).show()
        }
    }

    private fun getFirebaseNotifications() {
        val intent = intent
        val message = intent.getStringExtra("message")
        if(!message.isNullOrEmpty()) {
            AlertDialog.Builder(this)
                .setTitle("Notification")
                .setMessage(message)
                .setPositiveButton("Ok", { dialog, which -> }).show()
        }
    }

    private fun initilizeLogIn() {
        logIn.setOnClickListener {
            if (completeInfo()) {
                logIn()
            } else {
                Toast.makeText(this, "Completar datos", Toast.LENGTH_LONG).show()
            }

        }
    }

    private fun logIn() {
        var username = username.text.toString()
        var password = password.text.toString()
        val db = DBHelper(this)
        if (db.search(username, password)) {
            goToTournaments()
            finish()
        } else {
            Toast.makeText(this, "Datos incorrectos", Toast.LENGTH_LONG).show()
        }

    }

    private fun initilizeCreateUser() {
        createUser.setOnClickListener {
            goToCreateUser()
        }
    }

    private fun goToCreateUser() {
        val activityInsert = Intent(this, CreateUserActivity::class.java)
        startActivity(activityInsert)
    }

    private fun completeInfo(): Boolean {
        return !password.text.toString().isEmpty() &&
                !username.text.toString().isEmpty()
    }

    private fun initializeToolbar() {
        val toolbar: Toolbar = findViewById(R.id.toolbar_main)

        drawer = findViewById(R.id.drawer_layout)

        toggle = ActionBarDrawerToggle(this, drawer, toolbar, R.string.navigation_drawer_open, R.string.navigation_drawer_close)
        drawer.addDrawerListener(toggle)
        supportActionBar?.setDisplayHomeAsUpEnabled(true)
        supportActionBar?.setHomeButtonEnabled(true)

    }

    override fun onPostCreate(savedInstanceState: Bundle?) {
        super.onPostCreate(savedInstanceState)
        toggle.syncState()
    }

    override fun onConfigurationChanged(newConfig: Configuration?) {
        super.onConfigurationChanged(newConfig)
        toggle.onConfigurationChanged(newConfig)
    }

    override fun onNavigationItemSelected(item: MenuItem): Boolean {
        when (item.itemId) {
            R.id.navAbout -> goToAboutUs()
            R.id.nav_item_one -> goToSettings()
            R.id.navHelp -> showHelp()
        }
        return true
    }

    private fun setupUI() {
        username = findViewById(R.id.etUsername)
        password = findViewById(R.id.etPassword)
        logIn = findViewById(R.id.btnLogIn)
        createUser = findViewById(R.id.btnCreateUser)
        val navigationView: NavigationView = findViewById(R.id.nav_view)
        navigationView.setNavigationItemSelectedListener(this)
    }

    override fun onCreateOptionsMenu(menu: Menu?): Boolean {
        menuInflater.inflate(R.menu.activity_main_drawer, menu)
        return super.onCreateOptionsMenu(menu)
    }

    private fun showHelp() {
        var builder: AlertDialog.Builder = AlertDialog.Builder(this)
        builder.setTitle("Ayuda")
            .setMessage("Crea torneos y partidos!")
            .setPositiveButton("OK") { _, _ -> }
            .setCancelable(false)
            .show()
    }

    override fun onOptionsItemSelected(item: MenuItem): Boolean {
        if (toggle.onOptionsItemSelected(item)) {
            return true
        }
        when (item.itemId) {
            R.id.navAbout -> goToAboutUs()
            R.id.nav_item_one -> goToSettings()
            R.id.navHelp -> showHelp()
        }
        return super.onOptionsItemSelected(item)
    }

    private fun goToSettings() {
        val intent = Intent(this@MainActivity, SettingsActivity::class.java)
        startActivity(intent)
    }

    private fun asignarValoresSettings() {
        val pref = PreferenceManager.getDefaultSharedPreferences(this)

        nombre = pref.getString("etPrefNombre", "Anonimo")
        saludar = pref.getBoolean("chkPrefSaludar", false)
    }

    private fun goToAboutUs() {
        val activityInsert = Intent(this, AboutMeActivity::class.java)
        startActivity(activityInsert)
    }

    private fun goToTournaments() {
        val activityInsert = Intent(this, ListTournamentsActivity::class.java)
        startActivity(activityInsert)
    }

}


